import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import Cookies from "js-cookie";
import axios from "axios";
import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";


const RegisterForm = () => {
    const [responseError, setResponseError] = useState("");
    const navigate = useNavigate();
    const sessionID = Cookies.get("sessionID");

    const errors = {
        11000: "Użytkownik o takim loginie już istnieje."
    };

    const initialValues = {
        login: '',
        password: '',
        password_again: '',
        name: ''
    };

    const validateSchema = yup.object().shape({
        login: yup.string()
            .required("Login wymagany")
            .min(3, "Login musi zawierać minimum 3 znaki")
            .max(25, "Login musi zawierać maksymalnie 25 znaków"),

        password: yup.string()
            .required("Hasło wymagane")
            .min(6, "Hasło musi zawierać minimum 6 znaków"),

        password_again: yup.string()
            .test("same-password", "Hasła się nie zgadzają", function (value) {
                return value === this.from[0].value.password;
            }),

        name: yup.string()
            .required("Nazwa jest wymagana")
            .min(3, "Nazwa musi zawierać minimum 3 znaki")
            .max(25, "Nazwa może zawierać maksymalnie 25 znaków")
    });

    const handleSubmit = async (values, actions) => {
        if(!actions.isSubmitting){
            actions.setSubmitting(true);
            await axios.post("http://127.0.0.1:5000/users/", {
                login: values.login,
                password: values.password,
                name: values.name
            })
            .then(() => {navigate("/")})
            .catch(err => {setResponseError(errors[err.response.data.err.code]); actions.resetForm();})
            .finally(() => {actions.setSubmitting(false)});
        }
    }

    return(
        
        <div>
            {sessionID && <p>Jesteś zalogowany. Wyloguj się aby móc się rejestrować</p>}
            <Formik
            initialValues={initialValues}
            onSubmit={handleSubmit}   
            validationSchema={validateSchema}
            >
                {({isSubmitting}) => (<Form>
                    <label htmlFor="login">Login</label>
                    <Field name="login"></Field>
                    <ErrorMessage name="login"></ErrorMessage>

                    <label htmlFor="password">Hasło</label>
                    <Field name="password" type="password"></Field>
                    <ErrorMessage name="password"></ErrorMessage>

                    <label htmlFor="password_again">Powtórz hasło</label>
                    <Field name="password_again" type="password"></Field>
                    <ErrorMessage name="password_again"></ErrorMessage>

                    <label htmlFor="name">Nazwa</label>
                    <Field name="name"></Field>
                    <ErrorMessage name="name"></ErrorMessage>

                    <button type="submit" disabled={isSubmitting}>Zarejestruj</button>
                </Form>)}
            </Formik>
            {responseError && <p>{responseError}</p>}
        </div>
    );
}

export default RegisterForm