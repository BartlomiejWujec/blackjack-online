import axios from "axios";
import Cookies from "js-cookie";
import React, { useState, useEffect } from 'react';
import ReactModal from "react-modal";
import { Formik, Form, Field } from "formik";
import { useNavigate } from "react-router-dom";
import { authenticate, clearCookies } from "../utils/utils";
import "../styles/roomList.scss"

const RoomsList = () => {
    const [showModal, setShowModal] = useState(false);
    const [rooms, setRooms] = useState([]);
    const [errorMessage, setErrorMessage] = useState("");
    const [auth, setAuth] = useState(false);

    const errors = {
        "Room full": "Pokój pełen",
        "Already in this room": "Jesteś już w tym pokoju"
    }

    const navigate = useNavigate();

    const initialValues = {
        name: "",
        max_players: "2"
    }

    useEffect(() => {(async () => {
        if(!auth){
            authenticate().then(res => setAuth(res))
        }
        if(auth){
            await axios.get("http://127.0.0.1:5000/rooms")
                .then(res => setRooms(res.data.rooms))
                .catch(err => console.log(err));
        }
    })()}, [auth]);

    const addRoom = async values => {
        await axios.post("http://127.0.0.1:5000/rooms", values)
            .then(async res => {
                if(res.status === 200){
                    await axios.post(`http://127.0.0.1:5000/rooms/join/${res.data.id}`, {player_id: Cookies.get("player-id")})
                        .then(() => {
                            console.log(res.data);
                            Cookies.set("room", `${res.data.id}`, {expires: 365})   
                            navigate(`/rooms/${res.data.id}`)
                        })
                        .catch(err => {setErrorMessage(errors[err.response.data])});
                } else {
                    setErrorMessage("Nie udało się stworzyć pokoju");
                }
            })
            .catch(err => console.log(err))
    }

    return(
        <div className="roomsList">
            {errorMessage && <p>{errorMessage}</p>}
            {!auth && "Sesja wygasła"}
            {auth && 
            <div>
                <h1>Lista pokoi</h1>
                

                <button onClick={() => {
                    if(Cookies.get("room")) navigate(`/rooms/${Cookies.get("room")}`); 
                }}>Wróc do gry</button>

                <div>
                <h2>Dodaj pokój</h2>
                <button onClick={() => setShowModal(!showModal)}>+</button>
                <ReactModal
                    isOpen={showModal}
                    appElement={document.getElementById("root")}
                >
                    <button onClick={() => setShowModal(!showModal)}>x</button>
                    <Formik
                    initialValues={initialValues}
                    onSubmit={addRoom}
                    >
                        <Form>
                        <h1>Nazwa pokoju</h1>
                        <Field name="name"/>
                        <h1>Ilość graczy</h1>
                        <Field name="max_players" as="select">
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </Field>
                        <button type="submit">Stwórz pokój</button>
                        </Form>
                    </Formik>
                    
                </ReactModal>
                </div>
                <ul>
                    {rooms.map(room => {return(
                        <li key={room._id}>
                            {room.name}<br/>
                            {room.number_of_players}/{room.max_players}<br/>
                            <button onClick={async () => {
                                if(!Cookies.get("room")){
                                    await axios.post(`http://127.0.0.1:5000/rooms/join/${room._id}`, {player_id: Cookies.get("player-id")})
                                        .then(() => {
                                            Cookies.set("room", `${room._id}`, {expires: 365})   
                                            navigate(`/rooms/${room._id}`)
                                        })
                                        .catch(err => {setErrorMessage(errors[err.response.data])});
                                } else {
                                    setErrorMessage("Jesteś już w innym pokoju")
                                }
                                }}>Dołącz</button>    
                        </li>
                    );})}
                </ul>
            </div>}
        </div>
    );
};

export default RoomsList;