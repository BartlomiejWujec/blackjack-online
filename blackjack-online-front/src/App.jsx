import Cookies from "js-cookie";
import { Route, Routes, useNavigate } from "react-router-dom";
import RoomsList from "./components/RoomsList";
import LoginForm from "./components/login-system/LoginForm";
import RegisterForm from "./components/login-system/RegisterForm";
import Room from "./components/game/Room";
import axios from "axios";
import { clearCookies } from "./utils/utils";


function App() {
  const navigate = useNavigate();
  return (
    <div className="App">
      {Cookies.get("sessionID") && 
      <nav>
        <a href="/rooms">Lista pokoi</a>
        <button onClick={() => {
                    axios.post(`http://127.0.0.1:5000/users/logout/${Cookies.get("sessionID")}`)
                        .then(async () => {
                            await clearCookies()
                                .then(() => {
                                    navigate("/");
                                })
                        })
                }}>Wyloguj</button>
      </nav>}
      <Routes>
          <Route path="/" element={Cookies.get("sessionID") ? <RoomsList /> : <LoginForm />}/>
          <Route path="/register" element={<RegisterForm />}/>  
          <Route path="/rooms" element={<RoomsList />}/>
          <Route path="/rooms/:id" element={<Room />} />
      </Routes>
    </div>
  );
}

export default App;
