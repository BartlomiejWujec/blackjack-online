const express = require("express");
const router = express.Router();
const Login = require("../models/Login.js");
const Profile = require("../models/Profile.js");
const argon2 = require("argon2");

const sessions = new Set();


router.get("/:id", async (req, res) => {
    const { id } = req.params;
    const result = await Profile.find({"_id": id});
    res.status(200).send({ 
        user: result[0] 
    });
    return;
});

router.post("/", async (req, res) => {
    const { login, password, name } = req.body;
    let hash = undefined;

    try {
        hash = await argon2.hash(password);
    } catch(err) {
        console.log(err);
    }

    const loginToInsert = new Login({login, password: hash});
    await loginToInsert.save()
        .then(async loginResult => {
            const profileToInsert = new Profile({
                _id: loginResult._id,
                name: name
            })
            await profileToInsert.save()
            .then(() =>  {
                res.sendStatus(200);
            })
            .catch(err => {
                res.status(400).send({err})
            })
        })
        .catch(err => {
            res.status(400).send({err})
        });
    return;
});

router.delete("/:id", async (req, res) => {
    const { id } = req.params;
    const userToDelete = await Login.find({"_id": `${id}`});

    if(userToDelete.length === 0) { res.status(400).send("No such user"); return; }
    else {
        await Login.findByIdAndDelete(`${id}`)
            .then(async () => {
                await Profile.findByIdAndDelete(`${id}`)
                .then(() => {
                    res.sendStatus(200);
                })
                .catch(err => {
                    res.status(400).send({err})
                })
            })
            .catch(err => {
                res.status(400).send({err});
            });
    };
    return;
});

router.patch("/changepassword/:id", async (req, res) => {
    const { id } = req.params;
    const { password } = req.body;

    try {
        hash = await argon2.hash(password);
    } catch(err) {
        console.log(err);
    }

    const userToUpdate = await Login.find({"_id": `${id}`});
    if(userToUpdate.length === 0) { res.status(400).send("No such user"); return}
    else {
        await Login.findByIdAndUpdate(`${id}`, {...userToUpdate, password: hash})
            .then(() => {
                res.sendStatus(200);
            })
            .catch(err => {
                res.status(400).send({err: err});
            });
    };
    return;
});

router.post("/login", async (req, res) => {
    const { login, password } = req.body;
    const userToLogIn = await Login.find({"login": `${login}`});

    if(userToLogIn.length === 0) { res.status(400).send("User doesn't exist"); return};
    if(await argon2.verify(userToLogIn[0].password, password)){
        const sessionID = `${parseInt(Math.random()*10_000_000_000_000)}-${login}`;
        sessions.add(sessionID);
        console.log(sessions);
        res.status(200).send({sessionID, player_id: userToLogIn[0]._id})
    } else {
        res.status(400).send("Wrong password")
    };
    return;
});

router.post("/logout/:sessionID", async (req, res) => {
    const { sessionID } = req.params;
    if(sessions.has(sessionID)){
        sessions.delete(sessionID);
        console.log(sessions);
        res.sendStatus(200);
    } else res.sendStatus(400);
});

router.get("/auth/:sessionID", async (req, res) => {
    const { sessionID } = req.params;
    if(sessions.has(sessionID)){
        res.sendStatus(200);
    } else {
        res.sendStatus(403);
    }
});

module.exports = router;