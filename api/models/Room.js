const { Schema, model } = require('mongoose');

const RoomSchema = new Schema({
    name: {type: String, required: true, minlength: 3, maxlength: 25},
    number_of_players: {type: Number, min: 0, max: 4},
    max_players: {type: Number, min: 2, max: 4},
    players: [{type: String, ref: 'Profile'}]
}, {
    collection: 'Room'
})

module.exports = model('Room', RoomSchema)