const express = require('express');
const app = express();
const cors = require('cors');
const users = require("./routes/users.js");
const rooms = require("./routes/rooms.js");
const Room = require("./models/Room.js")

const cors_rules = {
    credentials: true,
    origin: "http://localhost:3000"
};

app.use(express.json());
app.use(cors(cors_rules));

app.use("/users", users);
app.use("/rooms", rooms);

const dbConnectData = {
    host: "127.0.0.1",
    port: 27017,
    database: "blackjack"
};

const mongoose = require('mongoose');
mongoose
    .connect(`mongodb://${dbConnectData.host}:${dbConnectData.port}/${dbConnectData.database}`)
    .then(res => {
        console.log(`Połączono z mongodb. Baza danych: ${res.connections[0].name}`);
        res.connection.dropCollection("Room");
        app.listen(5000, () => {
            console.log("Serwer API nasłuchuje na http://127.0.0.1:5000");
        })
    })