const express = require("express");
const router = express.Router();
const Room = require("../models/Room.js")

router.get("/", async (req, res) => {
    const result = await Room.find({});
    res.status(200).send({ 
        rooms: result 
    });
    return;
});

router.post("/", async (req, res) => {
    const { name, max_players } = req.body;
    const roomToCreate = new Room({name, max_players, number_of_players: 0})
    await roomToCreate.save()
        .then(result => {res.status(200).send({id: result._id})})
        .catch(err => {res.status(400).send({err})});
    return;
});

router.delete("/:id", async (req, res) => {
    const { id } = req.params;
    const roomToDelete = await Room.find({"_id": id});
    if(roomToDelete.length === 0) { res.status(400).send("No such room"); return; }
    else {
        await Room.findByIdAndDelete(id);
    }
});

router.patch("/:id", async (req, res) => {
    const { id } = req.params;
    const { name } = req.body;
    const roomToUpdate = await Room.find({"_id": id});
    if(roomToUpdate.length === 0) { res.status(400).send("No such room"); return; }
    else {
        await Room.findByIdAndUpdate(id, {...roomToUpdate[0], name})
            .then(() => {res.sendStatus(200)})
            .catch(err => {res.status(400).send({err})});
    }
    return;
});

router.get("/:id/players", async (req, res) => {
    const { id } = req.params;

    const roomToGet = await Room.find({"_id": id});
    if(roomToGet.length === 0) { res.status(400).send("No such room"); return; }
    else {
        const populated = await Room.populate(roomToGet[0], "players");
        res.status(200).send({populated})
    }
});

router.post("/leave/:id", async (req, res) => {
    const { id } = req.params;
    const { player_id } = req.body;
    
    const roomToLeave = await Room.find({"_id": id});
   
    if(roomToLeave.length === 0) { res.status(400).send("No such room"); return; }
    else {
        if(roomToLeave[0].number_of_players-1 === 0){
            await Room.findByIdAndDelete(id)
                .then(result => {res.sendStatus(200)})
                .catch(err => {res.status(400).send({err})});
        } else {
            await Room.findByIdAndUpdate(id, {"number_of_players": roomToLeave[0].number_of_players-1, players: roomToLeave[0].players.filter(x => {return String(x) !== String(player_id)})})
                .then(result => {res.sendStatus(200)})
                .catch(err => {res.status(400).send({err})});
        }
    }
    return;
});

router.post("/join/:id", async (req, res) => {
    const { player_id } = req.body;
    const { id } = req.params;

    const roomToJoin = await Room.find({"_id": id});
    if(roomToJoin.length === 0) { res.status(400).send("No such room"); return; }
    else if(roomToJoin[0].number_of_players+1>roomToJoin[0].max_players) {res.status(400).send("Room full"); return;}
    else if(roomToJoin[0].players.includes(player_id)) {res.status(400).send("Already in this room"); return;}
    else {
        await Room.findByIdAndUpdate(id, {"number_of_players": roomToJoin[0].number_of_players+1, "players": [...roomToJoin[0].players, player_id]})
            .then(() => {res.sendStatus(200)})
            .catch(err => {res.status(400).send({err})});
    }
    return;
});

module.exports = router;