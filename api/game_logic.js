const mqtt = require("mqtt");
const mqttClient = mqtt.connect("ws://127.0.0.1:8082/mqtt");
const Room = require("./models/Room.js")
const Profile = require("./models/Profile.js")

const randIndexFromArray= x => {
    return Math.round((Math.random()*Math.random()* 100000))%x.length;
}

const dealerPhase = async (dealer, topic) => {
    console.log(dealer);
    let index 
    let card
    while(dealer.value < 17){
        index = randIndexFromArray(dealer.deck);
        card = dealer.deck[index]
        dealer.hand = [...dealer.hand, card];
        dealer.deck.splice(index, 1);

        const x = card.slice(1)
        if(x !== "A"){
            dealer.value += values[x];
        } else {
            if(dealer.value + 11 > 21) dealer.value += 1;
            else dealer.value += 11;
        }
        mqttClient.publish(`${topic[0]}/${topic[1]}/cards`, `dealer/${JSON.stringify(games[topic[1]].dealer.hand)}`)
    }
}

const dbConnectData = {
    host: "127.0.0.1",
    port: 27017,
    database: "blackjack"
};
const values = {"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "10": 10, "J": 10, "Q": 10, "K": 10}
const deck = ['CA', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'CJ', 'CQ', 'CK', 'HA', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7', 'H8', 'H9', 'H10', 'HJ', 'HQ', 'HK', 'SA', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9', 'S10', 'SJ', 'SQ', 'SK', 'DA', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'DJ', 'DQ', 'DK']
const games = {}

const mongoose = require('mongoose');
mongoose
    .connect(`mongodb://${dbConnectData.host}:${dbConnectData.port}/${dbConnectData.database}`)
    .then(res => {
        console.log(`Połączono z mongodb. Baza danych: ${res.connections[0].name}`);
    })

mqttClient.on("connect", () => {
    console.log("Połączono z brokerem");
    mqttClient.subscribe("room/+/players");
    mqttClient.subscribe("room/+/start");
    mqttClient.subscribe("room/+/bet");
    mqttClient.subscribe("room/+/play")
});

mqttClient.on("message", async (t, m) => {
    const topic = t.split("/");
    if(topic[0] === "room"){
        if(topic[2] === "players"){
            if(m.toString() === "GET"){
                const populated = await Room.findById(topic[1]).populate("players").catch(err => console.log(err))
                
                if(populated) mqttClient.publish(`${t}`, JSON.stringify(populated.players))
            }
        }
        else if(topic[2] === "start"){
            if(m.toString() === "START"){
                const room = await Room.findById(topic[1])

                let hand = [];
                let new_deck = Array.from(deck);
                const  index = randIndexFromArray(new_deck)

                hand = [...hand, new_deck[index]];
                new_deck.splice(index, 1);
                const x = hand[0].slice(1)
                let value
                if(x === "A"){value = 11}
                else value = values[x] 
                

                games[topic[1]] = {...games[topic[1]], dealer: {hand, deck: new_deck, value}}
                games[topic[1]] = {...games[topic[1]], players_done: 0, currPlayers: room.players}

                for(const id of room.players){
                    const newObj = {...games[topic[1]]};
                    newObj[id] = {bet: 0}
                    games[topic[1]] = newObj
                }

                mqttClient.publish(`${topic[0]}/${topic[1]}/cards`, `dealer/${JSON.stringify(games[topic[1]].dealer.hand)}`)
            }
        }
        else if(topic[2] === "bet"){
            const message = m.toString().split("/");
            await Profile.findByIdAndUpdate(message[0], {"$inc": {"balance": -message[1]}});
            games[topic[1]][message[0]] = {...games[topic[1]][message[0]], bet: Number(message[1])};
        }
        else if(topic[2] === "play"){
            const message = m.toString().split("/");

            if(message[1] === "HIT"){
                const player = games[topic[1]][message[0]]

                const index = randIndexFromArray(player.deck)
                const card = player.deck[index]

                player.hand = [...player.hand, player.deck[index]];
                player.deck.splice(index, 1);

                const x = card.slice(1)
                if(x !== "A"){
                    player.value += values[x];
                } else {
                    mqttClient.publish(`${topic[0]}/${topic[1]}/play`, `${message[0]}/ACE`)
                }

                if(player.value > 21){
                    mqttClient.publish(t, `${message[0]}/STAND`)
                }
                mqttClient.publish(`${topic[0]}/${topic[1]}/cards`, `${message[0]}/${JSON.stringify(player.hand)}`)
            } 

            else if(message[1] === "STAND"){
                const game = games[topic[1]];

                game.players_done += 1;
                if(game.players_done >= game.currPlayers.length){
                    await dealerPhase(game.dealer, topic)
                        .then(() => {
                            let result
                            for(const player of game.currPlayers){
                                console.log(game[player].value, game.dealer.value);
                                if(game[player].value > 21){result = "loss";}
                                else if(game.dealer.value > 21){result = "win";}
                                else if(game[player].value < game.dealer.value){result = "loss";}
                                else {result = "win";}
                                mqttClient.publish(`${topic[0]}/${topic[1]}/result`, `${player}/${result}`);
                            }
                        })
                }
            }
            
            else if(message[1] === "ACE"){
                if(message[2]){
                    const player = games[topic[1]][message[0]];

                    player.value += Number(message[2])
                    if(player.value > 21){
                        mqttClient.publish(t, `${message[0]}/STAND`)
                    }
                }
            }

            else if(message[1] === "INIT"){
                const player = games[topic[1]][message[0]];

                let new_deck = Array.from(deck);
                let hand = [];

                let index = randIndexFromArray(new_deck)
                hand = [...hand, new_deck[index]];
                new_deck.splice(index, 1);

                index = randIndexFromArray(new_deck);
                hand = [...hand, new_deck[index]];
                new_deck.splice(index, 1);

                player.hand = hand;
                player.deck = new_deck;
                player.value = 0;

                if(hand[0][1] === "A" && hand[1][1] === "A") {
                    mqttClient.publish(`${topic[0]}/${topic[1]}/play`, `${message[0]}/2ACE`)
                }
                else {
                    for(const card of hand){
                        const x = card.slice(1)
                        if(x !== "A"){
                            player.value += values[x];
                        } else {
                            if(player.value === 10){
                                player.value += 11;
                            } else {
                                mqttClient.publish(`${topic[0]}/${topic[1]}/play`, `${message[0]}/ACE`)
                            }
                        }
                    }
                }
                mqttClient.publish(`${topic[0]}/${topic[1]}/cards`, `${message[0]}/${JSON.stringify(player.hand)}`)
            }
        }
    }
});