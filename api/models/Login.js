const { Schema, model } = require('mongoose');

const LoginSchema = new Schema({
    login: {type: String, required: true, unique: true, minlength: 3, maxlength: 25},
    password: {type: String, required: true, minlength: 6}
}, {
    collection: 'Login'
})

module.exports = model('Login', LoginSchema)