import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import axios from 'axios';
import mqtt from "mqtt";
import { useNavigate, useParams } from 'react-router-dom';
import { authenticate } from '../../utils/utils';
import ReactModal from 'react-modal';

const Room = () => {
    const [auth, setAuth] = useState(false);
    const [mqttClient, setMqttClient] = useState(null);
    const [players, setPlayers] = useState(null);
    const [player, setPlayer] = useState(null);
    const [messages, setMessages] = useState([]);
    const [faze, setFaze] = useState(0);
    const [bet, setBet] = useState(1);
    const [standing, setStanding] = useState(false);
    const [hand, setHand] = useState([]);
    const [dealer, setDealer] = useState([]);
    const [acePrompt, setAcePrompt] = useState(false);
    const [set1, setSet1] = useState(false);
    const [set2, setSet2] = useState(false);
    const [result, setResult] = useState("");

    const { id } = useParams();
    const navigate = useNavigate();
    const mqttPrefix = `room/${id}`;

    useEffect(() => {(async () => {
        if(!auth){
            authenticate().then(res => setAuth(res));
        }
        if(auth){
            await axios.get(`http://127.0.0.1:5000/users/${Cookies.get("player-id")}`)
                .then(res => setPlayer(res.data.user))
                .catch(err => console.log(err))

            setMqttClient(mqtt.connect("ws://127.0.0.1:8082/mqtt"))
        }
    })()}, [auth]);

    useEffect(() => {
        if(mqttClient){
            mqttClient.on("connect", () => {
                mqttClient.subscribe(`${mqttPrefix}/players`);
                mqttClient.subscribe(`${mqttPrefix}/chat`);
                mqttClient.subscribe(`${mqttPrefix}/start`);
                mqttClient.subscribe(`${mqttPrefix}/play`);
                mqttClient.subscribe(`${mqttPrefix}/cards`)
                mqttClient.subscribe(`${mqttPrefix}/result`)
                mqttClient.publish(`${mqttPrefix}/players`, "GET");
            });

            mqttClient.on("message", (t, m) => {
                const topic = t.split("/");
                const message = m.toString().split("/")
                if(topic[0] === "room"){    
                    if(topic[2] === "players"){
                        if(m.toString() !== "GET"){
                            setPlayers(JSON.parse(m.toString()));
                        }
                    }
                    else if(topic[2] === "chat"){
                        setMessages([...messages, m.toString()]);
                    }
                    else if(topic[2] === "start"){
                        setFaze(1)
                    }
                    else if(topic[2] === "play"){
                        if(message[0] === player._id && message[1] === "STAND"){
                            setStanding(true)
                        } else if(message[0] === player._id && (message[1] === "ACE" || message[1] === "2ACE") && !message[2]){
                            setAcePrompt(message[1]);
                        }
                    }
                    else if (topic[2] === "cards"){
                        if(message[0] === player._id){
                            setHand(JSON.parse(message[1]));
                        } else if (message[0] === "dealer"){
                            setDealer(JSON.parse(message[1]))
                        }
                    }
                    else if(topic[2] === "result"){
                        if(message[0] === player._id){
                            setTimeout(() => {
                                setFaze(3);
                                setResult(message[1] === "win" ? "Wygrana" : "Przegrana");
                            }, 1000*5)
                        }
                    }
                }
            });
        }
    }, [mqttClient, messages, mqttPrefix]);

    useEffect(() => {(async () => {
        if(faze === 1){
            setTimeout(() => {
                console.log("Czas obtawiania dobiegł końca");
                mqttClient.publish(`${mqttPrefix}/bet`, `${player._id}/${bet}`);
                setFaze(2);
                setStanding(false);
            }, 1000*1)
        } else if(faze === 2){
            mqttClient.publish(`${mqttPrefix}/play`, `${player._id}/INIT`);
            setBet(1);
        }
    })()}, [faze, bet]);

    const handleBet = betToAdd => {
        if(bet+betToAdd>0 && bet+betToAdd<=player.balance){
            setBet(bet+betToAdd)
        }
    }

    const handleSubmit = e => {
        e.preventDefault();
        mqttClient.publish(`${mqttPrefix}/chat`, `${player.name}: ${e.target.firstChild.value}`);
        e.target.firstChild.value = "";
    }

    return(
        <div>
            {!auth && "Sesja wygasła"}
            {auth && 
                <div>
                    {/* Wyjście z pokoju */}
                    <button onClick={async () => {
                        await axios.post(`http://127.0.0.1:5000/rooms/leave/${id}`, {player_id: Cookies.get("player-id")})
                            .then(res => {if(res.status === 200){
                                Cookies.remove("room");
                                mqttClient.publish(`${mqttPrefix}/players`, "GET");
                                navigate("/rooms");
                            }})
                            .catch(err => {
                                console.log(err);
                            })
                    }}>x</button>

                    {/* Start */}
                    <button onClick={() => { mqttClient.publish(`${mqttPrefix}/start`, "START")}}>start</button>

                    {/* Lista graczy */}
                    <h1>Gracze</h1>
                    <ul>
                        {players && players.map(player => {return(<li>{player.name} | {player.games_won}</li>)})}
                    </ul>

                    {/* Chat */}
                    <div>
                        <ul id='chatBox'>
                            {messages.map(x => {return(<p>{x}</p>)})}
                        </ul>
                        <form onSubmit={handleSubmit}>
                            <input type="text" id='chatInput'/>
                        </form>
                    </div>

                    {/* Stawki */}
                    {faze === 1 && 
                    <div>
                        <button onClick={() => {handleBet(1)}}>+1</button>
                        <button onClick={() => {handleBet(10)}}>+10</button>
                        <button onClick={() => {handleBet(100)}}>+100</button>
                        <span> {bet} </span>
                        <button onClick={() => {handleBet(-1)}}>-1</button>
                        <button onClick={() => {handleBet(-10)}}>-10</button>
                        <button onClick={() => {handleBet(-100)}}>-100</button>
                    </div>}

                    {/* Akcja graczy */}
                    {faze === 2 &&
                    <div>
                        <ReactModal
                        isOpen={acePrompt && true}
                        appElement={document.getElementById("root")}
                        >
                            {acePrompt === "ACE" && <div>
                                <button 
                                    onClick={() => {
                                        setAcePrompt(false); 
                                        mqttClient.publish(`${mqttPrefix}/play`, `${player._id}/ACE/1`);
                                    }}
                                >+1</button>
                                <button
                                    onClick={() => {
                                        setAcePrompt(false);
                                        mqttClient.publish(`${mqttPrefix}/play`, `${player._id}/ACE/11`)
                                    }}
                                >+11</button>
                            </div>}
                            {acePrompt === "2ACE" && <div>
                                <button 
                                    onClick={() => {
                                        setSet1(true)
                                        if(true && set2) {setAcePrompt(false); setSet2(false); setSet1(false)}; 
                                        mqttClient.publish(`${mqttPrefix}/play`, `${player._id}/ACE/1`);
                                    }}
                                    disabled={set1}
                                >+1</button>
                                <button
                                    onClick={() => {
                                        setSet1(true)
                                        if(true && set2) {setAcePrompt(false); setSet2(false); setSet1(false)}
                                        mqttClient.publish(`${mqttPrefix}/play`, `${player._id}/ACE/11`)
                                    }}
                                    disabled={set1}
                                >+11</button>
                                <button 
                                    onClick={() => {
                                        setSet2(true)
                                        if(set1 && true) {setAcePrompt(false); setSet2(false); setSet1(false)}; 
                                        mqttClient.publish(`${mqttPrefix}/play`, `${player._id}/ACE/1`);
                                    }}
                                    disabled={set2}
                                >+1</button>
                                <button
                                    onClick={() => {
                                        setSet2(true)
                                        if(set1 && true) {setAcePrompt(false); setSet2(false); setSet1(false)};
                                        mqttClient.publish(`${mqttPrefix}/play`, `${player._id}/ACE/11`)
                                    }}
                                    disabled={set2}
                                >+11</button>
                            </div>}
                        </ReactModal>
                        <h1>Krupier</h1>
                        {dealer.map(card => {return(<div>{card}</div>)})}
                        <h1>Twoja ręka</h1>
                        {hand.map(card => {return(<div>{card}</div>)})}
                        <button 
                        onClick={() =>{mqttClient.publish(`${mqttPrefix}/play`, `${player._id}/HIT`)}}
                        disabled={standing}
                        >HIT</button>
                        <button
                        onClick={() => {mqttClient.publish(`${mqttPrefix}/play`, `${player._id}/STAND`)}}
                        disabled={standing}
                        >STAND</button>
                    </div>}

                    {/* Wyniki */}
                    {faze === 3 &&
                    <div>
                        <h1>{result}</h1>
                    </div>}
                </div>
            }
        </div>
    );
}

export default Room