const { Schema, model } = require('mongoose');

const ProfileSchema = new Schema({
    name: {type: String, required: true, minlength: 3, maxlength: 25},
    description: {type: String, required: false, maxlength: 255},
    // profile_pic: {type: }
    games_won: {type: Number, default: 0, min: 0},
    balance: {type: Number, default: 0, min: 0},
    // comments: [{type: String, ref: }]
}, {
    collection: 'Profile'
})

module.exports = model('Profile', ProfileSchema)