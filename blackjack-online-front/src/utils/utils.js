import Cookies from "js-cookie"
import axios from "axios";

export const clearCookies = async () => {
    const allCookies = Object.keys(Cookies.get());
    for(const cookie of allCookies){
        Cookies.set(cookie, undefined, {expires: 0})
    }
    return;
}

export const authenticate = async () => {
    const sessionID = Cookies.get("sessionID");
    let return_value = false
    if(sessionID){ 
        await axios.get(`http://127.0.0.1:5000/users/auth/${sessionID}`)
            .then(res => {
                if(res.status === 200)
                    return_value = true
            })
            .catch(err => {
                console.log(err);
                clearCookies();
            });
    };
    return return_value;
}