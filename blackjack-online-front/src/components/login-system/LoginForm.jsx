import { Formik, Form, Field, ErrorMessage } from "formik";
import { useNavigate } from "react-router-dom";
import axios from "axios"
import Cookies from "js-cookie";
import "../../styles/loginForm.scss";

const LoginForm = () => {
    const initialValues = {
        login: "",
        password: ""
    }

    const navigate = useNavigate();

    const handleSubmit = (values, actions) => {(async (values, actions) => {
        axios.post("http://127.0.0.1:5000/users/login", values)
            .then(res => {
            if(res.status === 200 && res.data.sessionID && res.data.player_id){
                console.log(res.data);
                Cookies.set("sessionID", res.data.sessionID, { expires: 365 });
                Cookies.set("player-id", `${res.data.player_id}`, { expires: 365 });
                navigate("/rooms");
            }
        })
        .catch(err => console.log(err))
        .finally(() => {
            actions.setSubmitting(false);
        });
    })(values, actions)};
    return(
        <div className="container">
            <div className="title">BlackJack</div>
            <div className="loginFormContainer">
                
                <Formik 
                initialValues={initialValues}
                onSubmit={handleSubmit}
                >
                    {({ isSubmitting }) => (
                            <Form className="formikClass">
                                <div className="formLabels">
                                    <label htmlFor="login"> </label>
                                    <Field className='raberu' name="login" placeholder="Login"/>
                                    <ErrorMessage name="login"></ErrorMessage>
                                </div>
                                <div className="formLabels">
                                    <label htmlFor="password"></label>
                                    <Field className='raberu' name="password" type="password" placeholder="Hasło"/>
                                    <ErrorMessage name="password"></ErrorMessage>
                                </div>
                                <div className="submitButton">  
                                    <button disabled={isSubmitting} type="submit">Zaloguj</button>
                                </div>
                    </Form>)}
                </Formik>
            </div>
        </div>
    );
}

export default LoginForm;